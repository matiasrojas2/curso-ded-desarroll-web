<!DOCTYPE html>
<html>
<head>
	<title>estructuras de control en php</title>
</head>
<body>

<?php

/*las estructuras de control en lenguas de programacion
se utilizan para realizar operaciones por ejemplo: selectivas, repetitivas, y ambas;
las estructuras de control mas utilisadas son:

 *   IF (selectiva)
 *  FOR (repetitivas)
 *  WHILE (repetitivas-condicional)

 */

// estructura de control IF, ejemplo:
$x =1;
$y =10;
$z =5;

if ($x == 0) {
 echo "la variable es cero";
}
else{
	echo "la variable no es cero";

}
// estructura FOR, ejemplo:
$colores = array ('blue', 'geen', 'yellow', 'white', 'black', 'blue', 'green', 'orage', 'silver', 'white','black');

for ($i=0;$i <=10; $i++) {
	echo "<br />los numeros naturales hasta el 10" . $i;
}
for ($i=0; $i < 6 ; $i++){
	echo "<br />elemento $i =[ ". $colores [$i] ." ]";
}
?>
</html>